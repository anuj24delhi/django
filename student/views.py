from django.shortcuts import render, redirect
from django.http import *
from student.forms import *

# Create your views here.
def ssignup(request):
    if request.method=='POST':
        frm=studform(request.POST)
        print(frm)
        if frm.is_valid():
            frm.save()
            return redirect("/register")
    else:
        frm=studform()

    return render(request,'ssignup.html',{'stud':frm})