from django.db import models

# Create your models here.

class student(models.Model):
    sname=models.CharField(max_length=30,blank=False)
    fname=models.CharField(max_length=30,blank=False)
    mname=models.CharField(max_length=30,blank=False)
    sclass=models.IntegerField(blank=False)
    sno=models.CharField(max_length=10)
    sbirth=models.DateField()
    sgender=models.BooleanField()
    class Meta:
        db_table='student'


